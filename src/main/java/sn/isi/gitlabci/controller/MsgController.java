package sn.isi.gitlabci.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/")
public class MsgController {
    @GetMapping
    public String message() {
        return "Je suis la préférée de la famille...";
    }
}
